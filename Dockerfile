## docker build -t onquality/kraken-etl:1.0-SNAPSHOT .
## docker run -d --name KRAKEN_ETL -e MICRONAUT_PROFILE=redshift -p 8080:8080 onquality/kraken-etl:1.0-SNAPSHOT
## docker run -d --name KRAKEN_ETL -e MICRONAUT_PROFILE=redshift -p 8080:8080 254176233092.dkr.ecr.eu-central-1.amazonaws.com/onquality-kraken-etl-staging:latest

# BUILD JAR STAGE...
FROM maven:3.8-openjdk-17-slim AS buildStage
RUN mkdir /usr/src/etl-root
COPY . /usr/src/etl-root
WORKDIR /usr/src/etl-root/
RUN mvn clean package -B -DskipTests
RUN    mkdir /build
#RUN file2="$(pwd)" && echo $file2
#RUN file="$(ls -1)" && echo $file
#RUN echo $(ls -1)
RUN    mv /usr/src/etl-root/target/consolidation-app.jar /build/consolidation-app.jar
RUN    rm -rf /usr/src/etl-root/

# extract signed jar restriction files
FROM amazoncorretto:17.0.0-alpine as zipStage
COPY --from=buildStage /build/consolidation-app.jar /consolidation-app.jar
#RUN apk --no-cache add zip && \
#    zip -d /consolidation-app.jar 'META-INF/.SF' 'META-INF/.RSA' 'META-INF/*SF'
#
## we will use openjdk 17 with alpine as it is a very small linux distro
#FROM amazoncorretto:17.0.0-alpine as runStage
## copy the packaged jar file into our docker image
#COPY --from=zipStage /consolidation-app.jar /consolidation-app.jar

ENV AWS_CREDENTIAL_PROFILES_FILE='/home/app/.aws/credentials'
ENV AWS_REGION=eu-central-1
ENV S3_BUCKET=onquality-kraken-etl-staging
ENV S3_CONFIG_FILE='application-stockImports.yml'
ENV CONCURRENT_THREADS=3
ENV LOGBACK_FILE=logback-staging.xml

#EXPOSE 8080
#EXPOSE 5432

# set the startup command to execute the jar
# AWS_REGION=us-east-2;S3_CONFIG_FILE=application-s3.yml;S3_BUCKET=aws-kraken-staging
CMD java -jar -Ds3.bucket=$S3_BUCKET -Ds3.config.file=$S3_CONFIG_FILE -Daws.region=$AWS_REGION -Djava.util.concurrent.ForkJoinPool.common.parallelism=$CONCURRENT_THREADS -Dlogback.configurationFile=$LOGBACK_FILE -Xmx700m /consolidation-app.jar
#CMD java -jar -Xmx700m /consolidation-app.jar